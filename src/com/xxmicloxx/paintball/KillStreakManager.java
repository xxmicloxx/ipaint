/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xxmicloxx.paintball;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/**
 *
 * @author ml
 */
public class KillStreakManager {
    private Paintball plugin;
    private HashMap<KillStreak, Integer> kshm = new HashMap<KillStreak, Integer>();
    
    public KillStreakManager(Paintball plugin) {
        this.plugin = plugin;
    }
    
    public void addKillStreak(KillStreak ks, int kills) {
        kshm.put(ks, kills);
    }
    
    public void addKills(OwnPlayer p, int kills) {
        int killsOld = p.getPlayer().getLevel();
        int killsNew = killsOld + kills;
        p.getPlayer().setLevel(killsNew);
        int lowNum = 0;
        int highNum = Integer.MAX_VALUE;
        Set<Entry<KillStreak, Integer>> entrySet = kshm.entrySet();
        for (int i = 0; i < entrySet.size(); i++) {
            int num = ((Entry<KillStreak, Integer>)entrySet.toArray()[i]).getValue();
            if (num <= killsNew && num > lowNum) {
                lowNum = num;
            } else if (num > killsNew && num < highNum) {
                highNum = num;
            }
        }
        int diff = highNum - lowNum;
        float onePercent = (1.0f / diff);
        float percent = onePercent * (killsNew - lowNum);
        p.getPlayer().setExp(percent);
        for (int i = killsOld; i < killsNew; i++) {
            handleKills(p, i+1);
        }
    }
    
    public void resetKills(OwnPlayer p) {
        p.getPlayer().setLevel(0);
        p.getPlayer().setExp(0.00f);
    }
    
    private void handleKills(OwnPlayer p, int kills) {
        Iterator<Entry<KillStreak, Integer>> iterator = kshm.entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<KillStreak, Integer> entry = iterator.next();
            if (entry.getValue() == kills) {
                Player rp = p.getPlayer();
                plugin.chatHelper.asend(p, "&5&l&player&6&l got the \"&5&l" + entry.getKey().getStreakName() + "&6&l\" killstreak!");
                entry.getKey().handleKillStreak(p.getPlayer());
                Random d = new Random();
                rp.playSound(rp.getLocation(), Sound.CLICK, 1F, 0.4F / (d.nextFloat() * 0.4F + 0.8F));
            }
        }
    }
}
