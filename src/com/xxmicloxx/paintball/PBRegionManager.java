/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xxmicloxx.paintball;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 *
 * @author ML
 */
public class PBRegionManager {
    
    private static final String CONFIG_FILE = "arenas.yml";
    
    YamlConfiguration config;
    Paintball plugin;
    public PBRegionManager(Paintball plugin) {
        this.plugin = plugin;
        load();
        
        config.addDefault("cache.regions", new HashMap<String, HashMap<String, String>>());
        
        config.options().copyDefaults(true);
        save();
    }
    
    public void load() {
        File file = new File(plugin.getDataFolder(), CONFIG_FILE);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(PBRegionManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        config = YamlConfiguration.loadConfiguration(file);
    }
    
    public void save() {
        try {
            File file = new File(plugin.getDataFolder(), CONFIG_FILE);
            config.save(file);
        } catch (IOException ex) {
            Logger.getLogger(PBRegionManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void addRegion(String name, WorldRegion wr) {
        config.set("cache.regions." + name + ".region", wr.region);
        config.set("cache.regions." + name + ".world", wr.world);
        save();
    }
    
    public WorldRegion getWorldRegion(String name) {
        ConfigurationSection s = config.getConfigurationSection("cache.regions");
        if (s == null) {
            return null;
        }
        Map<String, Object> map = s.getValues(true);
        for (String str : map.keySet()) {
            if (str.equals(name)) {
                Map<String, Object> m = s.getConfigurationSection(str).getValues(false);
                return new WorldRegion((String) m.get("world"), (String) m.get("region"));
            }
        }
        return null;
    }
    
    public String getName(WorldRegion wr) {
        ConfigurationSection s = config.getConfigurationSection("cache.regions");
        if (s == null) {
            return null;
        }
        Map<String, Object> hm =  s.getValues(false);
        for (String sn : hm.keySet()) {
            Map<String, Object> wrmap = s.getConfigurationSection(sn).getValues(false);
            if (wrmap.get("world").equals(wr.world) && wrmap.get("region").equals(wr.region)) {
                return sn;
            }
        }
        return null;
    }
    
    public void removeRegion(String name) {
        config.set("cache.regions." + name, null);
        save();
    }
    
    public boolean checkIfNameExists(String name) {
        ConfigurationSection s = config.getConfigurationSection("cache.regions");
        if (s == null) {
            return false;
        }
        Map<String, Object> values = s.getValues(false);
        for (String sn : values.keySet()) {
            if (sn.equals(name)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean checkIfRegionExists(WorldRegion wr) {
        ConfigurationSection s = config.getConfigurationSection("cache.regions");
        Map<String, Object> hm = s.getValues(false);
        for (String sn : hm.keySet()) {
            Map<String, Object> hm2 = s.getConfigurationSection(sn).getValues(false);
            if (hm2.get("world").equals(wr.world) && hm2.get("region").equals(wr.region)) {
                return true;
            }
        }
        return false;
    }
}
