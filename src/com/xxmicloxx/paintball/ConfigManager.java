/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xxmicloxx.paintball;

import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;

/**
 *
 * @author ml
 */
public class ConfigManager {

    private Paintball plugin;
    private FileConfiguration config;

    public ConfigManager(Paintball plugin) {
        this.plugin = plugin;
        config = plugin.getConfig();
        plugin.getConfig().options().copyDefaults(true);

        config.addDefault("kill.effects.thunder", true);
        config.addDefault("kill.effects.firework.enabled", true);
        config.addDefault("kill.effects.explosion", true);
        config.addDefault("kill.effects.firework.power", 3);
        config.addDefault("kill.effects.fireworkblood", false);
        config.addDefault("game.effects.timechange.enable", false);
        config.addDefault("game.effects.timechange.timeinticks", 18000L);
        config.addDefault("game.chat.killmessage.enable", false);
        config.addDefault("game.rotate.enabled", false);
        config.addDefault("game.rotate.interval", 18000L);
        
        config.options().copyDefaults(true);
        
        plugin.saveConfig();
    }
    
    public void reload() {
        plugin.reloadConfig();
        config = plugin.getConfig();
    }
    
    public void setRotateEnabled(boolean enable) {
        config.set("game.rotate.enabled", enable);
        plugin.saveConfig();
    }
    
    public boolean getRotateEnabled() {
        return config.getBoolean("game.rotate.enabled");
    }
    
    public long getRotateDelay() {
        return config.getLong("game.rotate.interval");
    }
    
    public boolean getKillMessageEnabled() {
        return config.getBoolean("game.chat.killmessage.enable");
    }
    
    public boolean getTimechangeEnabled() {
        return config.getBoolean("game.effects.timechange.enable");
    }
    
    public long getTimechangeTime() {
        return config.getLong("game.effects.timechange.timeinticks");
    }
    
    public boolean getFireworkEnabled() {
        return config.getBoolean("kill.effects.firework.enabled");
    }
    
    public int getFireworkPower() {
        return config.getInt("kill.effects.firework.power");
    }
    
    public boolean getFireworkBlood() {
        return config.getBoolean("kill.effects.fireworkblood");
    }
    
    public boolean getThunderEnabled() {
        return config.getBoolean("kill.effects.thunder");
    }
    
    public boolean getExplosionEnabled() {
        return config.getBoolean("kill.effects.explosion");
    }

    public void setRegion(String name) {
        config.set("cache.currentgame.arena", name);
        plugin.saveConfig();
    }
    
    public String getRegion() {
        return config.getString("cache.currentgame.arena");
    }
    
    public void resetRegion() {
        config.set("cache.currentgame.arena", null);
        plugin.saveConfig();
    }
}
