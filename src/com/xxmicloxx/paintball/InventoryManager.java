/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xxmicloxx.paintball;

/**
 *
 * @author ml
 */
import org.bukkit.inventory.PlayerInventory;

public class InventoryManager {

    public static void clearInventory(PlayerInventory inv) {

        inv.setBoots(null);
        inv.setChestplate(null);
        inv.setHelmet(null);
        inv.setLeggings(null);
        inv.clear();
    }

    public static SavedInventory getInventoryContent(PlayerInventory inv) {
        int imax = inv.getSize();
        SavedInventory save = new SavedInventory(imax);
        for (int i = 0; i < imax; i++) {
            save.setItem(i, inv.getItem(i));
        }

        save.setHelmet(inv.getHelmet());
        save.setChestplate(inv.getChestplate());
        save.setLeggings(inv.getLeggings());
        save.setBoots(inv.getBoots());

        return save;
    }

    public static void setInventoryContent(SavedInventory save, PlayerInventory inv) {
        int imax = Math.min(save.getSize(), inv.getSize());
        for (int i = 0; i < imax; i++) {
            inv.setItem(i, save.getNewStackFrom(i));
        }

        inv.setBoots(save.getBoots());
        inv.setChestplate(save.getChestplate());
        inv.setHelmet(save.getHelmet());
        inv.setLeggings(save.getLeggings());
    }

    public static SavedInventory createDummyInventory() {
        return new SavedInventory(0);
    }
}