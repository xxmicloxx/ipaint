/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xxmicloxx.paintball;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import java.io.Serializable;
import org.bukkit.Bukkit;
import org.bukkit.World;

/**
 *
 * @author ml
 */
public class WorldRegion implements Serializable {
    private static final long serialVersionUID = 48644787324578L;
    
    public String world, region;
    
    public WorldRegion(World w, ProtectedRegion r) {
        this.world = w.getName();
        this.region = r.getId();
    }
    
    public WorldRegion(String w, String r) {
        this.world = w;
        this.region = r;
    }
    
    public World getWorld() {
        return Bukkit.getWorld(world);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WorldRegion other = (WorldRegion) obj;
        if ((this.world == null) ? (other.world != null) : !this.world.equals(other.world)) {
            return false;
        }
        if ((this.region == null) ? (other.region != null) : !this.region.equals(other.region)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + (this.world != null ? this.world.hashCode() : 0);
        hash = 37 * hash + (this.region != null ? this.region.hashCode() : 0);
        return hash;
    }
}
