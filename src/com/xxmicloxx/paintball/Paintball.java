/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xxmicloxx.paintball;

import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.bukkit.selections.CuboidSelection;
import com.sk89q.worldedit.bukkit.selections.Selection;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.databases.ProtectionDatabaseException;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.xxmicloxx.paintball.events.PaintballPlayerJoinEvent;
import com.xxmicloxx.paintball.events.PaintballPlayerLeaveEvent;
import com.xxmicloxx.paintball.gameobjects.*;
import com.xxmicloxx.paintball.killstreaks.MineKillStreak;
import com.xxmicloxx.paintball.killstreaks.ShotgunKillStreak;
import com.xxmicloxx.paintball.killstreaks.SniperKillStreak;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author ml
 */
public class Paintball extends JavaPlugin {

    public long gameTime = 0L;
    public Sponge sponge;
    public RotateManager rm;
    public RealGrenade rgr;
    public Shovel shovel;
    public PlayerListener pl;
    public ConfigManager cm;
    public ChatHelper chatHelper;
    public WorldGuardPlugin worldGuard;
    public Economy econ;
    public Grenade gr;
    public KillStreakManager ksm;
    private ShotgunKillStreak sks;
    private Shotgun sg;
    private MineKillStreak mks;
    private Mine m;
    private SniperKillStreak snks;
    private Sniper s;
    public PBRegionManager regm;
    public OwnedBlockManager obm;
    public ArrayList<OwnPlayer> arenaList = new ArrayList<OwnPlayer>();
    public HashMap<OwnPlayer, SavedInventory> invMap = new HashMap<OwnPlayer, SavedInventory>();
    public HashMap<OwnPlayer, Integer> healthValues = new HashMap<OwnPlayer, Integer>();
    public HashMap<OwnPlayer, IntFloat> xpValues = new HashMap<OwnPlayer, IntFloat>();
    public HashMap<OwnPlayer, IntFloat> hungerValues = new HashMap<OwnPlayer, IntFloat>();
    public HashMap<OwnPlayer, PotionEffect[]> potionValues = new HashMap<OwnPlayer, PotionEffect[]>();
    public HashMap<OwnPlayer, WorldVector> prevLocs = new HashMap<OwnPlayer, WorldVector>();
    public HashMap<OwnPlayer, Integer> prevGameMode = new HashMap<OwnPlayer, Integer>();
    public HashMap<WorldRegion, ArrayList<WorldVector>> spawnMap = new HashMap<WorldRegion, ArrayList<WorldVector>>();
    public ArrayList<OwnPlayer> playersHandling = new ArrayList<OwnPlayer>();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("You must be a Player!");
            return true;
        }
        Player p = (Player) sender;
        if (command.getName().equalsIgnoreCase("pb")) {
            switch (args.length) {
                case 0:
                    chatHelper.psend(p, "&b---- Help for iPaint ----\n&8<required> [optional]", true);
                    if (p.hasPermission("ipaint.setregion")) {
                        chatHelper.psend(p, "&5/pb setRegion <arena> &0| &dSet the current paintball arena", true);
                    }
                    if (p.hasPermission("ipaint.create")) {
                        chatHelper.psend(p, "&5/pb create <name> &0| &dCreate a new Arena based on your WE selection", true);
                    }
                    if (p.hasPermission("ipaint.remove")) {
                        chatHelper.psend(p, "&5/pb remove <name> &0| &dDelete an arena specified by it's name", true);
                    }
                    if (p.hasPermission("ipaint.rotate.add")) {
                        chatHelper.psend(p, "&5/pb rotate add <name> &0| &dAdd an arena to the rotation list", true);
                    }
                    if (p.hasPermission("ipaint.rotate.reset")) {
                        chatHelper.psend(p, "&5/pb rotate reset &0| &dReset the rotation list", true);
                    }
                    if (p.hasPermission("ipaint.rotate.rotate")) {
                        chatHelper.psend(p, "&5/pb rotate roate &0| &dRotate manually", true);
                    }
                    if (p.hasPermission("ipaint.rotate.enable")) {
                        chatHelper.psend(p, "&5/pb rotate enable/disable &0 &dEnable/disable automatic rotation");
                    }
                    if (p.hasPermission("ipaint.join")) {
                        chatHelper.psend(p, "&5/pb join &0| &dJoin iPaint", true);
                    }
                    if (p.hasPermission("ipaint.leave")) {
                        chatHelper.psend(p, "&5/pb leave &0| &dLeave iPaint", true);
                    }
                    if (p.hasPermission("ipaint.disable")) {
                        chatHelper.psend(p, "&5/pb disable &0| &dDisable iPaint", true);
                    }
                    if (p.hasPermission("ipaint.addspawn")) {
                        chatHelper.psend(p, "&5/pb addSpawn <arena> &0| &dAdds a spawnpoint for <region> at the current position", true);
                    }
                    if (p.hasPermission("ipaint.resetspawns")) {
                        chatHelper.psend(p, "&5/pb resetSpawns <arena> &0| &dReset all spawn points of <arena> (and disable it)", true);
                    }
                    if (p.hasPermission("ipaint.reload")) {
                        chatHelper.psend(p, "&5/pb reload &0| &dReload config files", true);
                    }
                    return true;
                case 1:
                    if (args[0].equalsIgnoreCase("join")) {
                        if (!p.hasPermission("ipaint.join")) {
                            chatHelper.psend(p, "&4You don't have permissions to execute this command!", true);
                            return true;
                        }
                        if (!isPBEnabled()) {
                            chatHelper.psend(p, "&4Paintball is currently disabled... Sorry!", true);
                            return true;
                        }
                        addPlayerToArena(p);
                        return true;
                    } else if (args[0].equalsIgnoreCase("leave")) {
                        if (!p.hasPermission("ipaint.leave")) {
                            chatHelper.psend(p, "&4You don't have permissions to execute this command!", true);
                            return true;
                        }
                        removePlayerFromArena(p);
                        return true;
                    } else if (args[0].equalsIgnoreCase("disable")) {
                        if (!p.hasPermission("ipaint.disable")) {
                            chatHelper.psend(p, "&4You don't have permissions to execute this command!", true);
                            return true;
                        }
                        disableArena();
                        chatHelper.psend(p, "&aSuccess! Paintball was disabled!", true);
                    } else if (args[0].equalsIgnoreCase("reload")) {
                        if (!p.hasPermission("ipaint.reload")) {
                            chatHelper.psend(p, "&4You don't have permissions to execute this command!", true);
                            return true;
                        }
                        cm.reload();
                        rm.load();
                        regm.load();
                        chatHelper.psend(p, "&aSuccess! Reloaded!", true);
                    } else if (args[0].equalsIgnoreCase("list")) {
                        if (!p.hasPermission("ipaint.list")) {
                            chatHelper.psend(p, "&4You don't have permissions to execute this command!", true);
                            return true;
                        }
                        String playerList = "&1List of players playing iPaint (" + arenaList.size() + "): ";
                        if (arenaList.isEmpty()) {
                            playerList += "&7none";
                        } else {
                            playerList += "&b" + arenaList.get(0).getPlayer().getName();
                        }
                        for (int i = 1; i < arenaList.size(); i++) {
                            Player player = arenaList.get(i).getPlayer();
                            playerList += "&f, &b" + player.getName();
                        }
                        chatHelper.psend(p, playerList, true);
                    }
                    break;
                case 2:
                    if (args[0].equalsIgnoreCase("streak")) {
                        if (!p.hasPermission("ipaint.streak")) {
                            chatHelper.psend(p, "&4You don't have permissions to execute this command!", true);
                            return true;
                        }
                        OwnPlayer ownP = new OwnPlayer(p);
                        if (!arenaList.contains(ownP)) {
                            return true;
                        }
                        int ks;
                        try {
                            ks = Integer.valueOf(args[1]);
                        } catch (NumberFormatException e) {
                            return true;
                        }
                        if (ks > 100) {
                            return true;
                        }
                        ksm.resetKills(ownP);
                        ksm.addKills(ownP, ks);
                    } else if (args[0].equalsIgnoreCase("rotate")) {
                        if (args[1].equalsIgnoreCase("reset")) {
                            if (!p.hasPermission("ipaint.rotate.reset")) {
                                chatHelper.psend(p, "&4You don't have permissions to execute this command!", true);
                                return true;
                            }
                            rm.resetRegions();
                            rm.save();
                            chatHelper.psend(p, "&aSuccess! Arenas reset!", true);
                        } else if (args[1].equalsIgnoreCase("rotate")) {
                            if (!p.hasPermission("ipaint.rotate.rotate")) {
                                chatHelper.psend(p, "&4You don't have permissions to execute this command!", true);
                                return true;
                            }
                            if (rm.getCount() < 2) {
                                chatHelper.psend(p, "&4Sorry, not enough arenass to rotate!", true);
                                return true;
                            }
                            if (!this.isPBEnabled()) {
                                changeMap(regm.getWorldRegion(rm.getRandomRegion()));
                            } else {
                                String name = rm.getRandomRegion();
                                WorldRegion wr = regm.getWorldRegion(name);
                                while (name.equals(cm.getRegion())) {
                                    name = rm.getRandomRegion();
                                    wr = regm.getWorldRegion(name);
                                }
                                changeMap(wr);
                            }
                            chatHelper.psend(p, "&aSuccess! Rotated!", true);
                        } else if (args[1].equalsIgnoreCase("enable")) {
                            if (!p.hasPermission("ipaint.rotate.enable")) {
                                chatHelper.psend(p, "&4You don't have permissions to execute this command!", true);
                                return true;
                            }
                            cm.setRotateEnabled(true);
                            chatHelper.psend(p, "&aSuccess! Rotating is enabled!", true);
                            gameTime = 0L;
                        } else if (args[1].equalsIgnoreCase("disable")) {
                            if (!p.hasPermission("ipaint.rotate.disable")) {
                                chatHelper.psend(p, "&4You don't have permissions to execute this command!", true);
                                return true;
                            }
                            cm.setRotateEnabled(false);
                            chatHelper.psend(p, "&aSuccess! Rotating is disabled!", true);
                        }
                    } else if (args[0].equalsIgnoreCase("setRegion")) {
                        if (!p.hasPermission("ipaint.setregion")) {
                            chatHelper.psend(p, "&4You don't have permissions to execute this command!", true);
                            return true;
                        }
                        WorldRegion wr = regm.getWorldRegion(args[1]);
                        if (wr == null) {
                            chatHelper.psend(p, "&4This arena does not exist!", true);
                            return true;
                        }
                        if (spawnMap.get(wr) == null || spawnMap.get(wr).isEmpty()) {
                            chatHelper.psend(p, "&4Add at least one spawn point!", true);
                            return true;
                        }
                        cm.setRegion(args[1]);
                        changeMap(wr);
                        chatHelper.psend(p, "&aSuccess! Arena set!", true);
                        return true;
                    } else if (args[0].equalsIgnoreCase("addspawn")) {
                        if (!p.hasPermission("ipaint.addspawn")) {
                            chatHelper.psend(p, "&4You don't have permissions to execute this command!", true);
                            return true;
                        }
                        WorldRegion wr = regm.getWorldRegion(args[1]);
                        if (wr == null) {
                            chatHelper.psend(p, "&4Arena not found!", true);
                            return true;
                        }
                        RegionManager regionManager = worldGuard.getRegionManager(wr.getWorld());
                        ProtectedRegion region = regionManager.getRegion(wr.region);
                        Location loc = p.getLocation();
                        if (!region.contains(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ())) {
                            chatHelper.psend(p, "&4You are not inside the specified arena!", true);
                            return true;
                        }
                        addSpawn(loc, wr);
                        chatHelper.psend(p, "&aSuccess! This spawnpoint was added!", true);
                        return true;
                    } else if (args[0].equalsIgnoreCase("resetspawns")) {
                        if (!p.hasPermission("ipaint.resetspawns")) {
                            chatHelper.psend(p, "&4You don't have permissions to execute this command!", true);
                            return true;
                        }
                        String name = cm.getRegion();
                        String selName = args[1];
                        WorldRegion wr = regm.getWorldRegion(args[1]);
                        if (wr == null) {
                            chatHelper.psend(p, "&4Arena not found!", true);
                            return true;
                        }
                        if (name.equals(selName)) {
                            disableArena();
                        }
                        spawnMap.remove(regm.getWorldRegion(selName));
                        chatHelper.psend(p, "&aSuccess! Arena spawns were reset!", true);
                        return true;
                    } else if (args[0].equalsIgnoreCase("create")) {
                        try {
                            if (!p.hasPermission("ipaint.create")) {
                                chatHelper.psend(p, "&4You don't have permissions to execute this command!", true);
                                return true;
                            }
                            String name = args[1];
                            if (regm.checkIfNameExists(name)) {
                                chatHelper.psend(p, "&4This name is already taken!", true);
                                return true;
                            }
                            RegionManager mgr = worldGuard.getRegionManager(p.getWorld());
                            WorldEditPlugin worldEdit = worldGuard.getWorldEdit();
                            Selection sel = worldEdit.getSelection(p);
                            ProtectedRegion region;
                            if ((sel instanceof CuboidSelection)) {
                                BlockVector min = sel.getNativeMinimumPoint().toBlockVector();
                                min = min.setY(0).toBlockVector();
                                BlockVector max = sel.getNativeMaximumPoint().toBlockVector();
                                max = max.setY(p.getWorld().getMaxHeight()).toBlockVector();
                                region = new ProtectedCuboidRegion("ipaint_" + name, min, max);
                            } else {
                                throw new CommandException("The type of region selected in WorldEdit is unsupported in WorldGuard!");
                            }
                            mgr.addRegion(region);
                            mgr.save();
                            WorldRegion wr = new WorldRegion(p.getWorld(), region);
                            regm.addRegion(name, wr);
                            regm.save();
                            chatHelper.psend(p, "&aSuccess! The arena has been created!");
                        } catch (ProtectionDatabaseException ex) {
                            Logger.getLogger(Paintball.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (CommandException ex) {
                            Logger.getLogger(Paintball.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else if (args[0].equalsIgnoreCase("remove")) {
                        if (!p.hasPermission("ipaint.remove")) {
                            chatHelper.psend(p, "&4You don't have permissions to execute this command!", true);
                            return true;
                        }
                        String name = args[1];
                        if (!regm.checkIfNameExists(name)) {
                            chatHelper.psend(p, "&4This arena does not exist!", true);
                            return true;
                        }
                        if (name.equals(cm.getRegion())) {
                            disableArena();
                        }
                        WorldRegion wr = regm.getWorldRegion(name);
                        World w = wr.getWorld();
                        RegionManager mgr = worldGuard.getRegionManager(w);
                        mgr.removeRegion(wr.region);
                        spawnMap.remove(wr);
                        rm.removeRotateRegion(name);
                        rm.save();
                        regm.removeRegion(name);
                        chatHelper.psend(p, "&aSuccess! This arena has been deleted!", true);
                    }
                    break;
                case 3:
                    if (args[0].equalsIgnoreCase("rotate")) {
                        if (args[1].equalsIgnoreCase("add")) {
                            if (!p.hasPermission("ipaint.rotate.add")) {
                                chatHelper.psend(p, "&4You don't have permissions to execute this command!", true);
                                return true;
                            }
                            WorldRegion wr = regm.getWorldRegion(args[2]);
                            if (wr == null) {
                                chatHelper.psend(p, "&4Arena not found!", true);
                                return true;
                            }
                            if (spawnMap.get(wr) == null || spawnMap.get(wr).isEmpty()) {
                                chatHelper.psend(p, "&4Add at least one spawn point!", true);
                                return true;
                            }
                            if (rm.checkIfRegionIsAdded(args[2])) {
                                chatHelper.psend(p, "&4This arena is already added!", true);
                                return true;
                            }
                            rm.addRotateRegion(args[2]);
                            rm.save();
                            chatHelper.psend(p, "&aSuccess! This arena was added!", true);
                            return true;
                        }
                    }
                    break;
            }
        }
        return false;
    }

    public void checkRegion() {
        int version = 0;
        File verFile = new File(this.getDataFolder(), "version.ser");
        try {
            if (!verFile.exists()) {
                verFile.createNewFile();
            } else {
                ObjectInputStream ois = new ObjectInputStream(new FileInputStream(verFile));
                version = ois.readInt();
                ois.close();
            }
        } catch (IOException ex) {
            Logger.getLogger(Paintball.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (version < 1) {
            //Version update!
            Logger.getLogger("iPaint").log(Level.WARNING, "iPaint has been updated, you have to recreate your regions and redefine your config!");
            File file = new File(this.getDataFolder(), "spawns.ser");
            file.delete();
            file = new File(this.getDataFolder(), "config.yml");
            file.delete();
            file = new File(this.getDataFolder(), "rotate.yml");
            file.delete();
        }
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(verFile));
            oos.writeInt(1);
            oos.flush();
            oos.close();
        } catch (IOException ex) {
            Logger.getLogger(Paintball.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ItemStack decrStackSize(ItemStack stk) {
        ItemStack newStack = stk.clone();
        if (newStack.getAmount() == 1) {
            newStack.setType(Material.AIR);
        } else {
            newStack.setAmount(newStack.getAmount() - 1);
        }
        return newStack;
    }

    public void changeMap(WorldRegion wr) {
        if (wr == null) {
            return;
        }
        obm.clearBlocks();
        cm.setRegion(regm.getName(wr));
        chatHelper.asend("&6The arena was changed!");
        Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {

            @Override
            public void run() {
                for (OwnPlayer ownP : arenaList) {
                    Player p = ownP.getPlayer();
                    if (p == null) {
                        continue;
                    }
                    randomTeleport(p);
                    p.setVelocity(new Vector());
                    InventoryManager.clearInventory(p.getInventory());
                    giveStartItems(ownP);
                    ksm.resetKills(ownP);
                }
            }
        });
    }

    public boolean isPBEnabled() {
        return cm.getRegion() != null;
    }

    public void disableArena() {
        cm.resetRegion();
        OwnPlayer[] array = new OwnPlayer[arenaList.size()];
        for (int i = 0; i < arenaList.size(); i++) {
            array[i] = arenaList.get(i);
        }
        for (OwnPlayer p : array) {
            removePlayerFromArena(p.getPlayer());
        }
    }

    public void addPlayerToArena(Player p) {
        OwnPlayer ownPlayer = new OwnPlayer(p);
        if (arenaList.contains(ownPlayer)) {
            chatHelper.psend(p, "&4You are already in the arena!");
            return;
        }
        arenaList.add(ownPlayer);
        if (cm.getTimechangeEnabled()) {
            p.setPlayerTime(cm.getTimechangeTime(), false);
        }
        chatHelper.psend(p, "&4This is a Pre-Release! &6Only playable at &bzeroxcraft.de &6and &bgommehd.tk&6!");
        saveItems(ownPlayer);
        giveStartItems(ownPlayer);
        randomTeleport(p);
        ksm.resetKills(ownPlayer);
        chatHelper.asend(ownPlayer, "&5&player&r joined Paintball!");
        PaintballPlayerJoinEvent pje = new PaintballPlayerJoinEvent(ownPlayer);
        Bukkit.getPluginManager().callEvent(pje);
    }

    public void randomTeleport(Player p) {
        Object[] wvObj = spawnMap.get(regm.getWorldRegion(cm.getRegion())).toArray();
        WorldVector[] wvArray = Arrays.copyOf(wvObj, wvObj.length, WorldVector[].class);
        Random random = new Random();
        int i = random.nextInt(wvArray.length);
        Location loc = wvArray[i].getLocation();
        Chunk chk = loc.getChunk();
        if (!chk.isLoaded()) {
            chk.load();
        }
        p.teleport(loc);
        //p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 2 * 20, 3));
        p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 3 * 20, 3));
        loc.getWorld().refreshChunk(loc.getChunk().getX(), loc.getChunk().getZ());
    }

    public void removePlayerFromArena(Player p) {
        OwnPlayer ownPlayer = new OwnPlayer(p);
        if (!arenaList.contains(ownPlayer)) {
            chatHelper.psend(p, "&4You are already outside of the arena!");
            return;
        }
        arenaList.remove(ownPlayer);
        restoreItems(ownPlayer);
        chatHelper.asend(ownPlayer, "&5&player&r left Paintball!");
        PaintballPlayerLeaveEvent ple = new PaintballPlayerLeaveEvent(ownPlayer);
        Bukkit.getPluginManager().callEvent(ple);
    }

    public void saveItems(OwnPlayer ownPlayer) {
        Player p = ownPlayer.getPlayer();
        invMap.put(ownPlayer, InventoryManager.getInventoryContent(p.getInventory()));
        healthValues.put(ownPlayer, p.getHealth());
        IntFloat xps = new IntFloat(p.getLevel(), p.getExp());
        xpValues.put(ownPlayer, xps);
        IntFloat hunger = new IntFloat(p.getFoodLevel(), p.getSaturation());
        hungerValues.put(ownPlayer, hunger);
        Object[] effectsObjects = p.getActivePotionEffects().toArray();
        PotionEffect[] effects = Arrays.copyOf(effectsObjects, effectsObjects.length, PotionEffect[].class);
        potionValues.put(ownPlayer, effects);
        for (PotionEffect effect : effects) {
            p.removePotionEffect(effect.getType());
        }
        WorldVector wv = new WorldVector(p.getLocation());
        prevLocs.put(ownPlayer, wv);
        prevGameMode.put(ownPlayer, p.getGameMode().getValue());
        InventoryManager.clearInventory(p.getInventory());
        p.setHealth(20);
        p.setFoodLevel(20);
        p.setSaturation(99999999999999F);
        p.setGameMode(GameMode.SURVIVAL);
    }

    public void giveStartItems(OwnPlayer player) {
        Player p = player.getPlayer();
        giveStartItems(p);
    }

    public void giveStartItems(Player p) {
        PlayerInventory inv = p.getInventory();
        LinkedList<ItemStack> isList = new LinkedList<ItemStack>();
        if (p.hasPermission("ipaint.keepstreaks")) {
            inv.remove(Material.IRON_SPADE);
            inv.remove(Material.SNOW_BALL);
            inv.remove(Material.EGG);
            for (ItemStack is : inv.getContents()) {
                if (is == null || is.getType() == Material.AIR) {
                    continue;
                }
                isList.add(is);
            }
        }
        InventoryManager.clearInventory(inv);
        ItemStack shovelStack = new ItemStack(Material.IRON_SPADE);
        ItemMeta meta = shovelStack.getItemMeta();
        meta.setDisplayName(ChatColor.AQUA + "" + ChatColor.BOLD + "Knife");
        shovelStack.setItemMeta(meta);
        inv.addItem(shovelStack);
        ItemStack snowItemStack = new ItemStack(Material.SNOW_BALL, 16);
        meta = snowItemStack.getItemMeta();
        meta.setDisplayName(ChatColor.AQUA + "" + ChatColor.BOLD + "Paintball");
        snowItemStack.setItemMeta(meta);
        for (int i = 0; i < 4; i++) {
            inv.setItem(i + 1, snowItemStack);
        }
        ItemStack eggItemStack = new ItemStack(Material.EGG);
        meta = eggItemStack.getItemMeta();
        meta.setDisplayName(ChatColor.AQUA + "" + ChatColor.BOLD + "Grenade");
        eggItemStack.setItemMeta(meta);
        inv.addItem(eggItemStack);
        Object[] effectsObjects = p.getActivePotionEffects().toArray();
        PotionEffect[] effects = Arrays.copyOf(effectsObjects, effectsObjects.length, PotionEffect[].class);
        for (PotionEffect effect : effects) {
            p.removePotionEffect(effect.getType());
        }
        p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 5 * 60 * 20, 3));
        p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 5 * 60 * 20, 3));
        for (ItemStack is : isList) {
            inv.addItem(is);
        }
        p.updateInventory();
    }

    public void fireworkExplosion(Player p) {
        if (cm.getFireworkEnabled()) {
            Random random = new Random();
            FireworkEffect.Type[] types = {FireworkEffect.Type.BALL_LARGE, FireworkEffect.Type.BURST};
            FireworkEffect effect = FireworkEffect.builder().with(types[random.nextInt(types.length)]).flicker(true).trail(true).withColor(Color.fromRGB(random.nextInt(128) + 127, random.nextInt(128) + 127, random.nextInt(128) + 127)).build();
            /*
             * FireworkEffect effect =
             * FireworkEffect.builder().with(FireworkEffect.Type.BALL).flicker(true).trail(true).withColor(Color.RED).build();
             */
            Firework f = (Firework) p.getWorld().spawnEntity(p.getLocation().add(0, 1, 0), EntityType.FIREWORK);
            FireworkMeta fm = (FireworkMeta) new ItemStack(Material.FIREWORK).getItemMeta();
            fm.setPower(cm.getFireworkPower());
            fm.addEffect(effect);
            f.setFireworkMeta(fm);
        }
        if (cm.getExplosionEnabled()) {
            Location loc = p.getLocation();
            p.getWorld().createExplosion(loc.getX(), loc.getY(), loc.getZ(), 5F, false, false);
        }
        if (cm.getThunderEnabled()) {
            p.getWorld().strikeLightningEffect(p.getLocation());
        }
        if (cm.getFireworkBlood()) {
            FireworkEffect effect = FireworkEffect.builder().with(FireworkEffect.Type.BALL).flicker(true).trail(true).withColor(Color.RED).build();
            Firework f = (Firework) p.getWorld().spawnEntity(p.getLocation().add(0, 1, 0), EntityType.FIREWORK);
            FireworkMeta fm = (FireworkMeta) new ItemStack(Material.FIREWORK).getItemMeta();
            fm.setPower(0);
            fm.addEffect(effect);
            f.setFireworkMeta(fm);
        }
    }

    public void restoreItems(OwnPlayer ownPlayer) {
        Player p = ownPlayer.getPlayer();
        p.resetPlayerTime();
        SavedInventory inv = invMap.get(ownPlayer);
        invMap.remove(ownPlayer);
        InventoryManager.clearInventory(p.getInventory());
        InventoryManager.setInventoryContent(inv, p.getInventory());
        int health = healthValues.get(ownPlayer);
        healthValues.remove(ownPlayer);
        p.setHealth(health);
        IntFloat xps = xpValues.get(ownPlayer);
        xpValues.remove(ownPlayer);
        p.setLevel(xps.getIntVal());
        p.setExp(xps.getFloatVal());
        IntFloat hunger = hungerValues.get(ownPlayer);
        hungerValues.remove(ownPlayer);
        p.setFoodLevel(hunger.getIntVal());
        p.setSaturation(hunger.getFloatVal());
        GameMode gm = GameMode.getByValue(prevGameMode.get(ownPlayer));
        prevGameMode.remove(ownPlayer);
        p.setGameMode(gm);
        Object[] effectsObjects = p.getActivePotionEffects().toArray();
        PotionEffect[] effectsdel = Arrays.copyOf(effectsObjects, effectsObjects.length, PotionEffect[].class);
        for (PotionEffect effect : effectsdel) {
            p.removePotionEffect(effect.getType());
        }
        if (potionValues.containsKey(ownPlayer)) {
            PotionEffect[] effects = potionValues.get(ownPlayer);
            potionValues.remove(ownPlayer);
            for (PotionEffect effect : effects) {
                p.addPotionEffect(effect);
            }
        }
        Location loc = prevLocs.get(ownPlayer).getLocation();
        prevLocs.remove(ownPlayer);
        Chunk chk = loc.getChunk();
        if (!chk.isLoaded()) {
            chk.load();
        }
        p.teleport(loc);
    }

    public void addSpawn(Location loc, WorldRegion wr) {
        ArrayList<WorldVector> spawns = spawnMap.get(wr);
        if (spawns == null) {
            spawns = new ArrayList<WorldVector>();
        }
        spawns.add(new WorldVector(loc));
        spawnMap.put(wr, spawns);
        saveSpawns();
    }

    @Override
    public void onDisable() {
        ObjectOutputStream oos = null;
        try {
            File invFile = new File(getDataFolder(), "states.ser");
            oos = new ObjectOutputStream(new FileOutputStream(invFile));
            SerializedHashMaps shm = new SerializedHashMaps(invMap, healthValues, xpValues, hungerValues, prevLocs, prevGameMode, arenaList);
            oos.writeObject(shm);
        } catch (IOException ex) {
            Logger.getLogger(Paintball.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                assert oos != null;
                oos.flush();
                oos.close();
            } catch (IOException ex) {
                Logger.getLogger(Paintball.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void saveSpawns() {
        ObjectOutputStream oos = null;
        try {
            File spawnFile = new File(getDataFolder(), "spawns.ser");
            oos = new ObjectOutputStream(new FileOutputStream(spawnFile));
            oos.writeObject(spawnMap);
        } catch (IOException ex) {
            Logger.getLogger(Paintball.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                assert oos != null;
                oos.flush();
                oos.close();
            } catch (IOException ex) {
                Logger.getLogger(Paintball.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void onEnable() {
        checkRegion();
        regm = new PBRegionManager(this);
        rm = new RotateManager(this);
        obm = new OwnedBlockManager(this);
        rgr = new RealGrenade(this);
        sponge = new Sponge(this);
        shovel = new Shovel(this);
        gr = new Grenade(this);
        pl = new PlayerListener(this);
        cm = new ConfigManager(this);
        ksm = new KillStreakManager(this);
        sks = new ShotgunKillStreak(this);
        sg = new Shotgun(this);
        mks = new MineKillStreak(this);
        m = new Mine(this);
        snks = new SniperKillStreak(this);
        s = new Sniper(this);
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {

            @Override
            public void run() {
                gameTime += 20;
                if (rm.getCount() > 1 && isPBEnabled() && cm.getRotateEnabled() && gameTime >= cm.getRotateDelay()) {
                    System.out.println(cm.getRotateDelay());
                    String name = rm.getRandomRegion();
                    while (name.equals(cm.getRegion())) {
                    }
                    changeMap(regm.getWorldRegion(name));
                    gameTime = 0L;
                }
            }
        }, 20, 20);
        worldGuard = (WorldGuardPlugin) getServer().getPluginManager().getPlugin("WorldGuard");
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            getLogger().warning("[iPaint]Vault not found, disabling econ functions...");
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            getLogger().warning("[iPaint]Economy plugin not found, disabling econ functions...");
        } else {
            econ = rsp.getProvider();
            if (econ == null) {
                getLogger().warning("[iPaint]Economy plugin not found, disabling econ functions...");
            }
        }
        chatHelper = new ChatHelper(this);
        File invFile = new File(getDataFolder(), "states.ser");
        if (!invFile.exists()) {
            try {
                // create file
                invFile.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(Paintball.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                ObjectInputStream ois = new ObjectInputStream(new FileInputStream(invFile));
                SerializedHashMaps hm = (SerializedHashMaps) ois.readObject();
                this.invMap = hm.inventories;
                this.healthValues = hm.healthValues;
                this.hungerValues = hm.hungerValues;
                this.xpValues = hm.xpValues;
                this.prevLocs = hm.prevPos;
                this.prevGameMode = hm.prevGamemode;
                //this.arenaList = hm.playerList;
                ois.close();
                for (Player p : Bukkit.getOnlinePlayers()) {
                    OwnPlayer ownPlayer = new OwnPlayer(p);
                    if (!arenaList.contains(ownPlayer) && invMap.containsKey(ownPlayer)) {
                        restoreItems(ownPlayer);
                    }
                }
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Paintball.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Paintball.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        File regionsFile = new File(getDataFolder(), "spawns.ser");
        if (!regionsFile.exists()) {
            try {
                regionsFile.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(Paintball.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                ObjectInputStream ois = new ObjectInputStream(new FileInputStream(regionsFile));
                this.spawnMap = (HashMap<WorldRegion, ArrayList<WorldVector>>) ois.readObject();
                ois.close();
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Paintball.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Paintball.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (spawnMap == null) {
            spawnMap = new HashMap<WorldRegion, ArrayList<WorldVector>>();
            saveSpawns();
        }
    }
}
