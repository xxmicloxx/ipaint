/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xxmicloxx.paintball;

import java.io.Serializable;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 *
 * @author ml
 */
public class OwnPlayer implements Serializable {
    private static final long serialVersionUID = 63094580348953L;
    
    public String player;
    
    public OwnPlayer(Player player) {
        this(player.getName());
    }
    
    public OwnPlayer(String player) {
        this.player = player;
    }
    
    public Player getPlayer() {
        return Bukkit.getPlayer(player);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OwnPlayer other = (OwnPlayer) obj;
        if ((this.player == null) ? (other.player != null) : !this.player.equals(other.player)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + (this.player != null ? this.player.hashCode() : 0);
        return hash;
    }
}
