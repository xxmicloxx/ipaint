/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xxmicloxx.paintball;

import java.io.Serializable;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;

/**
 *
 * @author ml
 */
public class WorldVector implements Serializable {
    private static final long serialVersionUID = 1486398496345L;
    
    public double x, y, z;
    public float pitch, yaw;
    
    public String world;
    
    public WorldVector(Location loc) {
        this(loc.toVector(), loc.getWorld(), loc.getPitch(), loc.getYaw());
    }
    
    public WorldVector(Vector v, World w, float pitch, float yaw) {
        world = w.getName();
        x = v.getX();
        y = v.getY();
        z = v.getZ();
        this.pitch = pitch;
        this.yaw = yaw;
    }
    
    public Vector getVector() {
        return new Vector(x, y, z);
    }
    
    public Location getLocation() {
        return new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
    }
    
    public World getWorld() {
        return Bukkit.getWorld(world);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (int) (Double.doubleToLongBits(this.x) ^ (Double.doubleToLongBits(this.x) >>> 32));
        hash = 41 * hash + (int) (Double.doubleToLongBits(this.y) ^ (Double.doubleToLongBits(this.y) >>> 32));
        hash = 41 * hash + (int) (Double.doubleToLongBits(this.z) ^ (Double.doubleToLongBits(this.z) >>> 32));
        hash = 41 * hash + Float.floatToIntBits(this.pitch);
        hash = 41 * hash + Float.floatToIntBits(this.yaw);
        hash = 41 * hash + (this.world != null ? this.world.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WorldVector other = (WorldVector) obj;
        if (Double.doubleToLongBits(this.x) != Double.doubleToLongBits(other.x)) {
            return false;
        }
        if (Double.doubleToLongBits(this.y) != Double.doubleToLongBits(other.y)) {
            return false;
        }
        if (Double.doubleToLongBits(this.z) != Double.doubleToLongBits(other.z)) {
            return false;
        }
        if (Float.floatToIntBits(this.pitch) != Float.floatToIntBits(other.pitch)) {
            return false;
        }
        if (Float.floatToIntBits(this.yaw) != Float.floatToIntBits(other.yaw)) {
            return false;
        }
        if ((this.world == null) ? (other.world != null) : !this.world.equals(other.world)) {
            return false;
        }
        return true;
    }
}
