package com.xxmicloxx.paintball;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class SerializedHashMaps implements Serializable{

	private static final long serialVersionUID = -829296935480224825L;

	public HashMap<OwnPlayer, SavedInventory> inventories = new HashMap<OwnPlayer, SavedInventory>();
	public HashMap<OwnPlayer, Integer> healthValues = new HashMap<OwnPlayer, Integer>();
	public HashMap<OwnPlayer, IntFloat> xpValues = new HashMap<OwnPlayer, IntFloat>();
	public HashMap<OwnPlayer, IntFloat> hungerValues = new HashMap<OwnPlayer, IntFloat>();
        public HashMap<OwnPlayer, WorldVector> prevPos = new HashMap<OwnPlayer, WorldVector>();
        public HashMap<OwnPlayer, Integer> prevGamemode = new HashMap<OwnPlayer, Integer>();
        public ArrayList<OwnPlayer> playerList = new ArrayList<OwnPlayer>();

	public SerializedHashMaps(HashMap<OwnPlayer, SavedInventory> inventories,
			HashMap<OwnPlayer, Integer> healthValues,
			HashMap<OwnPlayer, IntFloat> xpValues,
			HashMap<OwnPlayer, IntFloat> hungerValues,
                        HashMap<OwnPlayer, WorldVector> prevPos,
                        HashMap<OwnPlayer, Integer> prevGamemode,
                        ArrayList<OwnPlayer> playerList) {
		this.inventories = inventories;
		this.healthValues = healthValues;
		this.xpValues = xpValues;
		this.hungerValues = hungerValues;
                this.prevPos = prevPos;
                this.prevGamemode = prevGamemode;
                this.playerList = playerList;
	}

}