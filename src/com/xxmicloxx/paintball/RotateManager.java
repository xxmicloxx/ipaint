/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xxmicloxx.paintball;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 *
 * @author ML
 */
public class RotateManager {

    private static final String CONFIG_FILE = "rotate.yml";
    YamlConfiguration config;
    Paintball plugin;

    public RotateManager(Paintball plugin) {
        this.plugin = plugin;
        load();
    }

    public void load() {
        File file = new File(plugin.getDataFolder(), CONFIG_FILE);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(RotateManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        config = YamlConfiguration.loadConfiguration(file);
    }

    public void save() {
        try {
            File file = new File(plugin.getDataFolder(), CONFIG_FILE);
            config.save(file);
        } catch (IOException ex) {
            Logger.getLogger(RotateManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int getCount() {
        List<String> list = config.getStringList("cache.rotate.maps");
        if (list == null) {
            return 0;
        } else {
            return list.size();
        }
    }

    public boolean checkIfRegionIsAdded(String str) {
        List<String> regions = config.getStringList("cache.rotate.maps");
        if (regions == null) {
            return false;
        }
        return regions.contains(str);
    }

    public void addRotateRegion(String region) {
        List<String> regions = config.getStringList("cache.rotate.maps");
        if (regions == null) {
            regions = new LinkedList<String>();
        }
        regions.add(region);
        config.set("cache.rotate.maps", regions);
    }
    
    public void removeRotateRegion(String region) {
        List<String> regions = config.getStringList("cache.rotate.maps");
        if (regions == null) {
            return;
        }
        regions.remove(region);
        config.set("cache.rotate.maps", regions);
    }

    public void resetRegions() {
        config.set("cache.rotate.maps", new LinkedList<String>());
    }

    public String getRandomRegion() {
        Random random = new Random();
        List<String> list = config.getStringList("cache.rotate.maps");
        if (list.isEmpty()) {
            return null;
        }
        int randNum = random.nextInt(list.size());
        String text = list.get(randNum);
        return text;
    }
}
