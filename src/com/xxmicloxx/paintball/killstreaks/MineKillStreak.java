/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xxmicloxx.paintball.killstreaks;

import com.xxmicloxx.paintball.KillStreak;
import com.xxmicloxx.paintball.Paintball;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author ML
 */
public class MineKillStreak extends KillStreak {
     
    public MineKillStreak(Paintball plugin) {
         super(plugin);
         plugin.ksm.addKillStreak(this, 3);
     }

    @Override
    public String getStreakName() {
        return "Mine";
    }

    @Override
    public void handleKillStreak(Player attacker) {
        ItemStack mine = new ItemStack(Material.STONE_PLATE);
        ItemMeta meta = mine.getItemMeta();
        meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Mine");
        mine.setItemMeta(meta);
        attacker.getInventory().addItem(mine);
    }
}
