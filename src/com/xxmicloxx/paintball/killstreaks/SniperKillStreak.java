/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xxmicloxx.paintball.killstreaks;

import com.xxmicloxx.paintball.KillStreak;
import com.xxmicloxx.paintball.Paintball;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author ML
 */
public class SniperKillStreak extends KillStreak {
    public SniperKillStreak(Paintball plugin) {
        super(plugin);
        plugin.ksm.addKillStreak(this, 9);
    }

    @Override
    public String getStreakName() {
        return "MG";
    }

    @Override
    public void handleKillStreak(Player attacker) {
        ItemStack mg = new ItemStack(Material.IRON_HOE);
        ItemMeta meta = mg.getItemMeta();
        meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "MG");
        mg.setItemMeta(meta);
        attacker.getInventory().addItem(mg);
    }
}
