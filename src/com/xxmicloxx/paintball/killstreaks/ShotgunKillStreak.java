/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xxmicloxx.paintball.killstreaks;

import com.xxmicloxx.paintball.KillStreak;
import com.xxmicloxx.paintball.Paintball;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author ML
 */
public class ShotgunKillStreak extends KillStreak {
    public ShotgunKillStreak(Paintball plugin) {
        super(plugin);
        plugin.ksm.addKillStreak(this, 6);
    }
    
    @Override
    public void handleKillStreak(Player attacker) {
        ItemStack hoe = new ItemStack(Material.STONE_HOE);
        ItemMeta meta = hoe.getItemMeta();
        meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Shotgun");
        hoe.setItemMeta(meta);
        attacker.getInventory().addItem(hoe);
    }

    @Override
    public String getStreakName() {
        return "Shotgun";
    }
}
