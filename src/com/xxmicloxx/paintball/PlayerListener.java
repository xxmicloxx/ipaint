/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xxmicloxx.paintball;

import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.xxmicloxx.paintball.events.PaintballPlayerLeaveEvent;
import java.util.Arrays;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.*;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 *
 * @author ml
 */
public class PlayerListener implements Listener {
    
    private Paintball plugin;
    
    public PlayerListener(Paintball plugin) {
        this.plugin = plugin;
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }
    
    @EventHandler
    public void onCommandPreprocess(PlayerCommandPreprocessEvent e) {
        Player p = e.getPlayer();
        OwnPlayer ownP = new OwnPlayer(p);
        String cmd = e.getMessage().split(" ")[0];
        System.out.println(cmd);
        if (plugin.arenaList.contains(ownP) && !p.hasPermission("ipaint.commandallow") && !cmd.trim().equalsIgnoreCase("/pb")) {
            e.setCancelled(true);
        }
    }
    
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        OwnPlayer ownPlayer = new OwnPlayer(e.getPlayer());
        if (!plugin.arenaList.contains(ownPlayer) && plugin.invMap.containsKey(ownPlayer)) {
            plugin.restoreItems(ownPlayer);
        }
    }
    
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        OwnPlayer ownPlayer = new OwnPlayer(e.getPlayer());
        if (plugin.arenaList.contains(ownPlayer)) {
            plugin.arenaList.remove(ownPlayer);
            plugin.chatHelper.asend(ownPlayer, "&5&player&r left Paintball!");
            PaintballPlayerLeaveEvent event = new PaintballPlayerLeaveEvent(ownPlayer);
            Bukkit.getPluginManager().callEvent(event);
        }
    }
    
    @EventHandler
    public void onEntityDamage(EntityDamageEvent e) {
        if (e.getEntityType() != EntityType.PLAYER) {
            return;
        }
        Player p = (Player) e.getEntity();
        OwnPlayer ownP = new OwnPlayer(p);
        if (!plugin.arenaList.contains(ownP)) {
            return;
        }
        e.setDamage(0);
    }
    
    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent e) {
        Player p = e.getPlayer();
        OwnPlayer ownP = new OwnPlayer(p);
        if (!plugin.arenaList.contains(ownP)) {
            return;
        }
        Object[] wvObj = plugin.spawnMap.get(plugin.regm.getWorldRegion(plugin.cm.getRegion())).toArray();
        WorldVector[] wvArray = Arrays.copyOf(wvObj, wvObj.length, WorldVector[].class);
        Random random = new Random();
        int i = random.nextInt(wvArray.length);
        Location loc = wvArray[i].getLocation();
        Chunk chk = loc.getChunk();
        if (!chk.isLoaded()) {
            chk.load();
        }
        //p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 2 * 20, 3));
        p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 3 * 20, 3));
        e.setRespawnLocation(loc);
        plugin.giveStartItems(p);
    }
    
    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        Player p = e.getPlayer();
        OwnPlayer ownPlayer = new OwnPlayer(p);
        if (!plugin.arenaList.contains(ownPlayer)) {
            return;
        }
        WorldRegion wr = plugin.regm.getWorldRegion(plugin.cm.getRegion());
        World w = wr.getWorld();
        RegionManager mgr = plugin.worldGuard.getRegionManager(w);
        ProtectedRegion region = mgr.getRegion(wr.region);
        Location loc = e.getTo();
        if (region.contains(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ())) {
            return;
        }
        plugin.randomTeleport(p);
        plugin.chatHelper.psend(p, "&4You tried to leave the arena!\n&rYou are getting respawned...");
    }
    
    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e) {
        Player p = e.getEntity();
        OwnPlayer ownP = new OwnPlayer(p);
        if (!plugin.arenaList.contains(ownP)) {
            return;
        }
        e.getDrops().clear();
        e.setDroppedExp(0);
    }
    
    @EventHandler
    public void onItemDrop(PlayerDropItemEvent e) {
        Player p = e.getPlayer();
        OwnPlayer ownPlayer = new OwnPlayer(p);
        if (!plugin.arenaList.contains(ownPlayer)) {
            return;
        }
        e.setCancelled(true);
    }
}
