/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xxmicloxx.paintball.events;

import com.xxmicloxx.paintball.OwnPlayer;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 *
 * @author ML
 */
public class PaintballPlayerJoinEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private OwnPlayer ownP;
    
    public PaintballPlayerJoinEvent(OwnPlayer ownP) {
        super();
        this.ownP = ownP;
    }
    
    public OwnPlayer getPlayer() {
        return ownP;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
    
    public static HandlerList getHandlerList() {
        return handlers;
    }
}
