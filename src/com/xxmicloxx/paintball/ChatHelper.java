/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xxmicloxx.paintball;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 *
 * @author ml
 */
public class ChatHelper {

    private Paintball plugin;
    private final static String PREFIX = ChatColor.RESET + "[" + ChatColor.DARK_BLUE + "iPaint" + ChatColor.RESET + "] ";
    private final static String PREFIX_INT = ChatColor.RESET + "[" + ChatColor.LIGHT_PURPLE + ChatColor.BOLD + "iPaint" + ChatColor.RESET + "] ";
    
    public ChatHelper(Paintball plugin) {
        this.plugin = plugin;
    }

    public void psend(OwnPlayer p, String msg, boolean intended) {
        msg = convertCodes(msg, p);
        String pre;
        if (intended) {
            pre = PREFIX_INT;
        } else {
            pre = PREFIX;
        }
        p.getPlayer().sendMessage(pre + msg);
    }
    
    public void psend(Player p, String msg, boolean intended) {
        psend(new OwnPlayer(p.getName()), msg, intended);
    }
    
    public void psend(OwnPlayer p, String msg) {
        boolean intended = false;
        if (msg.contains(p.player)) {
            intended = true;
        }
        psend(p, msg, intended);
    }
    
    public void psend(Player p, String msg) {
        psend(new OwnPlayer(p), msg);
    }
    
    public void asend(OwnPlayer p, String msg) {
        msg = convertCodes(msg, p);
        for (OwnPlayer arpl : plugin.arenaList) {
            boolean intended = false;
            if (msg.contains(arpl.player)) {
                intended = true;
            }
            psend(arpl, msg, intended);
        }
    }
    
    public void asend(Player p, String msg) {
        asend(new OwnPlayer(p), msg);
    }
    
    public void asend(String msg) {
        msg = convertCodes(msg);
        for (OwnPlayer ownP : plugin.arenaList) {
            Player p = ownP.getPlayer();
            p.getPlayer().sendMessage(PREFIX_INT + msg);
        }
    }
    
    public void psend(OwnPlayer attacker, OwnPlayer victim, OwnPlayer p, String msg, boolean intended) {
        msg = convertCodes(msg, attacker, victim);
        String pre;
        if (intended) {
            pre = PREFIX_INT;
        } else {
            pre = PREFIX;
        }
        p.getPlayer().sendMessage(pre + msg);
    }
    
    public void psend(Player attacker, Player victim, Player p, String msg, boolean intended) {
        psend(new OwnPlayer(attacker), new OwnPlayer(victim), new OwnPlayer(p), msg, intended);
    }
    
    public void psend(OwnPlayer attacker, OwnPlayer victim, OwnPlayer p, String msg) {
        boolean intended = false;
        if (msg.contains(p.player)) {
            intended = true;
        }
        psend(attacker, victim, p, msg, intended);
    }
    
    public void psend(Player attacker, Player victim, Player p, String msg) {
        psend(new OwnPlayer(attacker), new OwnPlayer(victim), new OwnPlayer(p), msg);
    }
    
    public void asend(OwnPlayer attacker, OwnPlayer victim, String msg) {
        msg = convertCodes(msg, attacker, victim);
        for (OwnPlayer arpl : plugin.arenaList) {
            boolean intended = false;
            if (msg.contains(arpl.player)) {
                intended = true;
            }
            psend(attacker, victim, arpl, msg, intended);
        }
    }
    
    public void asend(Player attacker, Player victim, String msg) {
        asend(new OwnPlayer(attacker), new OwnPlayer(victim), msg);
    }
    
    public static String convertCodes(String str) {
        str = str.replaceAll("&0", ChatColor.BLACK + "");
        str = str.replaceAll("&1", ChatColor.DARK_BLUE + "");
        str = str.replaceAll("&2", ChatColor.DARK_GREEN + "");
        str = str.replaceAll("&3", ChatColor.DARK_AQUA + "");
        str = str.replaceAll("&4", ChatColor.DARK_RED + "");
        str = str.replaceAll("&5", ChatColor.DARK_PURPLE + "");
        str = str.replaceAll("&6", ChatColor.GOLD + "");
        str = str.replaceAll("&7", ChatColor.GRAY + "");
        str = str.replaceAll("&8", ChatColor.DARK_GRAY + "");
        str = str.replaceAll("&9", ChatColor.BLUE + "");
        str = str.replaceAll("&a", ChatColor.GREEN + "");
        str = str.replaceAll("&b", ChatColor.AQUA + "");
        str = str.replaceAll("&c", ChatColor.RED + "");
        str = str.replaceAll("&d", ChatColor.LIGHT_PURPLE + "");
        str = str.replaceAll("&e", ChatColor.YELLOW + "");
        str = str.replaceAll("&f", ChatColor.WHITE + "");
        str = str.replaceAll("&k", ChatColor.MAGIC + "");
        str = str.replaceAll("&l", ChatColor.BOLD + "");
        str = str.replaceAll("&m", ChatColor.STRIKETHROUGH + "");
        str = str.replaceAll("&n", ChatColor.UNDERLINE + "");
        str = str.replaceAll("&o", ChatColor.ITALIC + "");
        str = str.replaceAll("&r", ChatColor.RESET + "");
        str = str.replaceAll("\n", "\n" + PREFIX);
        
        str = removeCodes(str);
        return str;
    }

    public static String convertCodes(String str, OwnPlayer p) {
        str = str.replaceAll("&player", p.player);

        str = convertCodes(str);
        return str;
    }

    public static String convertCodes(String str, OwnPlayer attacker, OwnPlayer victim) {
        if (attacker != null) {
            str = str.replaceAll("&attacker", attacker.player);
        }
        if (victim != null) {
            str = str.replaceAll("&victim", victim.player);
        }

        str = convertCodes(str);
        return str;
    }

    public static String removeCodes(String str) {
        str = str.replaceAll("&0", "");
        str = str.replaceAll("&1", "");
        str = str.replaceAll("&2", "");
        str = str.replaceAll("&3", "");
        str = str.replaceAll("&4", "");
        str = str.replaceAll("&5", "");
        str = str.replaceAll("&6", "");
        str = str.replaceAll("&7", "");
        str = str.replaceAll("&8", "");
        str = str.replaceAll("&9", "");
        str = str.replaceAll("&a", "");
        str = str.replaceAll("&b", "");
        str = str.replaceAll("&c", "");
        str = str.replaceAll("&d", "");
        str = str.replaceAll("&e", "");
        str = str.replaceAll("&f", "");
        str = str.replaceAll("&k", "");
        str = str.replaceAll("&l", "");
        str = str.replaceAll("&m", "");
        str = str.replaceAll("&n", "");
        str = str.replaceAll("&o", "");
        str = str.replaceAll("&r", "");

        return str;
    }
}
