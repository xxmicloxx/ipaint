/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xxmicloxx.paintball;

import com.xxmicloxx.paintball.events.PaintballPlayerLeaveEvent;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.event.server.PluginEnableEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

/**
 * @author ML
 */
public class OwnedBlockManager implements Listener {
    private Paintball plugin;
    public HashMap<Block, OwnPlayer> blockMap = new HashMap<Block, OwnPlayer>();

    public OwnedBlockManager(Paintball plugin) {
        this.plugin = plugin;
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    public void addHandledBlock(Block b, OwnPlayer p) {
        blockMap.put(b, p);
    }

    @EventHandler
    public void onPlayerLeave(PaintballPlayerLeaveEvent e) {
        ArrayList<Block> blockList = new ArrayList<Block>();
        Set<Entry<Block, OwnPlayer>> entrySet = blockMap.entrySet();
        for (Entry<Block, OwnPlayer> next : entrySet) {
            if (next.getValue().player.equals(e.getPlayer().player)) {
                blockList.add(next.getKey());
            }
        }
        for (Block b : blockList) {
            b.setType(Material.AIR);
            blockMap.remove(b);
        }
    }

    @EventHandler
    public void onPluginDisable(PluginDisableEvent e) {
        clearBlocks();
    }

    @EventHandler
    public void onPluginEnable(PluginEnableEvent e) {
        clearBlocks();
    }

    public OwnPlayer getOwner(Block b) {
        return blockMap.get(b);
    }

    public void clearBlocks() {
        Set<Block> keySet = blockMap.keySet();
        for (Block b : keySet) {
            b.setType(Material.AIR);
        }
    }
}
