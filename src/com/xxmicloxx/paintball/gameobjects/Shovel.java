/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xxmicloxx.paintball.gameobjects;

import com.xxmicloxx.paintball.InventoryManager;
import com.xxmicloxx.paintball.OwnPlayer;
import com.xxmicloxx.paintball.Paintball;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

/**
 *
 * @author ml
 */
public class Shovel implements Listener {

    private Paintball plugin;

    public Shovel(Paintball plugin) {
        this.plugin = plugin;
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onShovelHit(EntityDamageByEntityEvent e) {
        if (e.getCause() != DamageCause.ENTITY_ATTACK) {
            return;
        }
        if (e.getEntityType() != EntityType.PLAYER) {
            return;
        }
        final Player p = (Player) e.getEntity();
        final OwnPlayer ownP = new OwnPlayer(p);
        if (!plugin.arenaList.contains(ownP)) {
            return;
        }
        Entity damager = e.getDamager();
        if (damager.getType() != EntityType.PLAYER) {
            return;
        }
        final Player attacker = (Player) damager;
        OwnPlayer ownA = new OwnPlayer(attacker);
        if (!plugin.arenaList.contains(ownA)) {
            return;
        }
        if (attacker.getItemInHand().getType() != Material.IRON_SPADE) {
            return;
        }
        if (plugin.playersHandling.contains(ownP)) {
            return;
        }
        plugin.playersHandling.add(ownP);
        e.setDamage(0);
        final ItemStack shovel = attacker.getItemInHand();
        short newDur = (short) (shovel.getDurability() + 84);
        shovel.setDurability(newDur);
        if (newDur <= 251) {
            p.setItemInHand(new ItemStack(Material.AIR));
        }
        plugin.fireworkExplosion(p);
        plugin.randomTeleport(p);
        p.setVelocity(new Vector());
        plugin.giveStartItems(ownP);
        plugin.playersHandling.remove(ownP);
        if (plugin.cm.getKillMessageEnabled()) {
            plugin.chatHelper.asend(attacker, p, "&5&attacker&r hit &5&victim&r!");
        }
        plugin.ksm.addKills(ownA, 2);
        plugin.ksm.resetKills(ownP);
    }
}
