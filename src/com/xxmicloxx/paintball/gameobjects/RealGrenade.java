/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xxmicloxx.paintball.gameobjects;

import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.xxmicloxx.paintball.OwnPlayer;
import com.xxmicloxx.paintball.Paintball;
import com.xxmicloxx.paintball.WorldRegion;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Egg;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.util.Vector;

/**
 *
 * @author ml
 */
public class RealGrenade implements Listener {

    private Paintball plugin;

    public RealGrenade(Paintball plugin) {
        this.plugin = plugin;
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void eggSpawnDisable(CreatureSpawnEvent e) {
        if (e.getSpawnReason() != SpawnReason.EGG) {
            return;
        }
        WorldRegion wr = plugin.regm.getWorldRegion(plugin.cm.getRegion());
        World w = wr.getWorld();
        RegionManager mgr = plugin.worldGuard.getRegionManager(w);
        ProtectedRegion pr = mgr.getRegion(wr.region);
        if (pr == null) {
            return;
        }
        Location loc = e.getLocation();
        if (loc.getWorld() != wr.getWorld()) {
            return;
        }
        if (!pr.contains(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ())) {
            return;
        }
        e.setCancelled(true);
    }

    @EventHandler
    public void grenadeHit(ProjectileHitEvent e) {
        if (e.getEntityType() != EntityType.EGG) {
            return;
        }
        final Egg egg = (Egg) e.getEntity();
        final LivingEntity shooter = egg.getShooter();
        if (shooter.getType() != EntityType.PLAYER) {
            return;
        }
        Player attacker = (Player) shooter;
        OwnPlayer ownA = new OwnPlayer(attacker);
        if (!plugin.arenaList.contains(ownA)) {
            return;
        }
        final World w = attacker.getWorld();
        final Random random = new Random();
        final int max = 65;
        final Location loc = egg.getLocation();
        final Snowball ball[] = new Snowball[max];
        for (int i = 0; i < max; i++) {
            ball[i] = (Snowball) w.spawnEntity(loc, EntityType.SNOWBALL);
        }
        for (int i = 0; i < max; i++) {
            ball[i].setShooter(shooter);
            Vector vel = new Vector((random.nextFloat() * 2.0) - 1.0, (random.nextFloat() * 2.0) - 1.0, (random.nextFloat() * 2.0) - 1.0);
            vel.multiply(random.nextFloat());
            ball[i].setVelocity(vel);
        }
        w.createExplosion(loc.getX(), loc.getY(), loc.getZ(), 2F, false, false);
    }
}
