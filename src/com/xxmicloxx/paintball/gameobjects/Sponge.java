/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xxmicloxx.paintball.gameobjects;

import com.xxmicloxx.paintball.InventoryManager;
import com.xxmicloxx.paintball.OwnPlayer;
import com.xxmicloxx.paintball.Paintball;
import java.util.ArrayList;
import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author ml
 */
public class Sponge implements Listener {
    
    private Paintball plugin;
    private HashMap<OwnPlayer, Integer> threadMap = new HashMap<OwnPlayer, Integer>();
    private HashMap<OwnPlayer, Integer> tickMap = new HashMap<OwnPlayer, Integer>();
    
    public Sponge(Paintball plugin) {
        this.plugin = plugin;
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }
    
    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        final Player p = e.getPlayer();
        final OwnPlayer ownPlayer = new OwnPlayer(p);
        if (!plugin.arenaList.contains(ownPlayer)) {
            return;
        }
        final Location loc = p.getLocation().subtract(0, 1, 0);
        if (p.getWorld().getBlockAt(loc).getType() != Material.SPONGE) {
            return;
        }
        final PlayerInventory inv = p.getInventory();
        if (inv.contains(Material.SNOW_BALL)) {
            return;
        }
        ArrayList<ItemStack> prevItems = new ArrayList<ItemStack>();
        if (inv.getContents() != null) {
            for (ItemStack is : inv.getContents()) {
                if ((is == null) || (is.getType() == Material.AIR)) {
                    continue;
                }
                prevItems.add(is);
            }
        }
        InventoryManager.clearInventory(inv);
        ItemStack snowItemStack = new ItemStack(Material.SNOW_BALL, 16);
        ItemMeta meta = snowItemStack.getItemMeta();
        meta.setDisplayName(ChatColor.AQUA + "" + ChatColor.BOLD + "Paintball");
        for (int i = 0; i < 4; i++) {
            inv.setItem(i+1, snowItemStack);
        }
        for (ItemStack is : prevItems) {
            inv.addItem(is);
        }
        if (inv.contains(Material.EGG)) {
            return;
        }
        tickMap.put(ownPlayer, 20*3);
        int taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {

            @Override
            public void run() {
                if (!plugin.arenaList.contains(ownPlayer)) {
                    int thread = threadMap.get(ownPlayer);
                    threadMap.remove(ownPlayer);
                    tickMap.remove(ownPlayer);
                    Bukkit.getScheduler().cancelTask(thread);
                    return;
                }
                if (p.getWorld().getBlockAt(p.getLocation().subtract(0, 1, 0)).getType() != Material.SPONGE) {
                    int thread = threadMap.get(ownPlayer);
                    threadMap.remove(ownPlayer);
                    tickMap.remove(ownPlayer);
                    Bukkit.getScheduler().cancelTask(thread);
                    return;
                }
                int ticksLeft = tickMap.get(ownPlayer);
                ticksLeft--;
                tickMap.put(ownPlayer, ticksLeft);
                if (ticksLeft == 0) {
                    int thread = threadMap.get(ownPlayer);
                    threadMap.remove(ownPlayer);
                    tickMap.remove(ownPlayer);
                    Bukkit.getScheduler().cancelTask(thread);
                    ItemStack eggStack = new ItemStack(Material.EGG);
                    ItemMeta meta = eggStack.getItemMeta();
                    meta.setDisplayName(ChatColor.AQUA + "" + ChatColor.BOLD + "Grenade");
                    eggStack.setItemMeta(meta);
                    inv.addItem(eggStack);
                }
            }
        }, 2, 1);
        threadMap.put(ownPlayer, taskId);
    }
}
