/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xxmicloxx.paintball.gameobjects;

import com.xxmicloxx.paintball.OwnPlayer;
import com.xxmicloxx.paintball.Paintball;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

/**
 *
 * @author ML
 */
public class Shotgun implements Listener {
    private Paintball plugin;
    
    public Shotgun(Paintball plugin) {
        this.plugin = plugin;
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }
    
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        if (e.getAction() != Action.RIGHT_CLICK_AIR && e.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return;
        }
        Player p = e.getPlayer();
        OwnPlayer ownP = new OwnPlayer(p);
        if (!plugin.arenaList.contains(ownP)) {
            return;
        }
        ItemStack hoe = e.getItem();
        if (hoe == null || hoe.getType() != Material.STONE_HOE) {
            return;
        }
        //checks done, go on with code
        final World w = p.getWorld();
        final Location eyeLoc = p.getEyeLocation();
        Vector veltemp = eyeLoc.getDirection();
        final Vector vel = veltemp.multiply(3);
        final Random r = new Random();
        final Snowball[] snowballs = new Snowball[16];
        for (int i = 0; i < snowballs.length; i++) {
            snowballs[i] = (Snowball) w.spawnEntity(eyeLoc, EntityType.SNOWBALL);
            snowballs[i].setShooter(p);
        }
                for (int i = 0; i < snowballs.length; i++) {
                    Snowball currBall = snowballs[i];
                    Vector currVel = vel.clone();
                    float modX = (r.nextFloat() / 2.5f) - 0.25f;
                    float modY = (r.nextFloat() / 2.5f) - 0.25f;
                    float modZ = (r.nextFloat() / 2.5f) - 0.25f;
                    float modRange = (r.nextFloat()) + 0.5f;
                    currVel.add(new Vector(modX, modY, modZ));
                    currVel.multiply(modRange);
                    currBall.setVelocity(currVel);
                }
                w.createExplosion(eyeLoc, -1.0f);
        short newDur = (short) (hoe.getDurability() + 44);
        hoe.setDurability(newDur);
        if (newDur >= 132) {
            p.setItemInHand(new ItemStack(Material.AIR));
        }
    }
}
