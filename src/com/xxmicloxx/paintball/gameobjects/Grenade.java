/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xxmicloxx.paintball.gameobjects;

import com.xxmicloxx.paintball.InventoryManager;
import com.xxmicloxx.paintball.OwnPlayer;
import com.xxmicloxx.paintball.Paintball;
import org.bukkit.Bukkit;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.util.Vector;

/**
 *
 * @author ml
 */
public class Grenade implements Listener {

    private Paintball plugin;

    public Grenade(Paintball plugin) {
        this.plugin = plugin;
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onEntityDamageByEntity(final EntityDamageByEntityEvent e) {
        if (e.getEntityType() != EntityType.PLAYER) {
            return;
        }
        //System.out.println(e.getEntityType());
        final Player p = (Player) e.getEntity();
        if (e.getDamager().getType() != EntityType.SNOWBALL) {
            return;
        }
        //System.out.println(e.getDamager().getType());
        final OwnPlayer ownPlayer = new OwnPlayer(p);
        if (!plugin.arenaList.contains(ownPlayer)) {
            return;
        }
        Snowball ball = (Snowball) e.getDamager();
        LivingEntity livEntAtt = (LivingEntity) ball.getShooter();
        if (livEntAtt.getType() != EntityType.PLAYER) {
            return;
        }
        Player attacker = (Player) livEntAtt;
        OwnPlayer ownPAtt = new OwnPlayer(attacker);
        if (!plugin.arenaList.contains(ownPAtt)) {
            return;
        }
        if (plugin.playersHandling.contains(ownPlayer)) {
            return;
        }
        plugin.playersHandling.add(ownPlayer);
        plugin.fireworkExplosion(p);
        plugin.randomTeleport(p);
        p.setVelocity(new Vector());
        plugin.giveStartItems(ownPlayer);
        plugin.playersHandling.remove(ownPlayer);
        if (plugin.cm.getKillMessageEnabled()) {
            plugin.chatHelper.asend(attacker, p, "&5&attacker&r hit &5&victim&r!");
        }
        plugin.ksm.addKills(ownPAtt, 1);
        plugin.ksm.resetKills(ownPlayer);
    }
}
