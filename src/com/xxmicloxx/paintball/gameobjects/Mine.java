/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xxmicloxx.paintball.gameobjects;

import com.xxmicloxx.paintball.OwnPlayer;
import com.xxmicloxx.paintball.Paintball;
import java.util.Random;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

/**
 *
 * @author ML
 */
public class Mine implements Listener {

    private Paintball plugin;

    public Mine(Paintball plugin) {
        this.plugin = plugin;
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerInteract(PlayerInteractEvent e) {
        if (e.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return;
        }
        Player p = e.getPlayer();
        OwnPlayer ownP = new OwnPlayer(p);
        if (!plugin.arenaList.contains(ownP)) {
            return;
        }
        ItemStack mine = e.getItem();
        if (mine == null || mine.getType() != Material.STONE_PLATE) {
            return;
        }
        e.setCancelled(true);
        Block clickedBlock = e.getClickedBlock();
        Block placeBlock = clickedBlock.getRelative(e.getBlockFace());
        Block blockUnder = placeBlock.getRelative(0, -1, 0);
        if (!blockUnder.getType().isSolid() || blockUnder.getType() == Material.SPONGE || placeBlock.getType() != Material.AIR) {
            return;
        }
        placeBlock.setType(Material.STONE_PLATE);
        World w = placeBlock.getWorld();
        w.playSound(placeBlock.getLocation(), Sound.FUSE, 1.0F, 0.5F);
        plugin.obm.addHandledBlock(placeBlock, ownP);
        p.setItemInHand(plugin.decrStackSize(mine));
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        final Player p = e.getPlayer();
        final OwnPlayer ownP = new OwnPlayer(p);
        if (!plugin.arenaList.contains(ownP)) {
            return;
        }
        final Location locat = p.getLocation();
        final World w = p.getWorld();
        Block plate = w.getBlockAt(locat);
        if (plate.getType() != Material.STONE_PLATE) {
            return;
        }
        final OwnPlayer owner = plugin.obm.getOwner(plate);
        final Player own = owner.getPlayer();
        final Location loc = locat.add(0.5, 0.5, 0.5);
        final Snowball ball[] = new Snowball[32];
        for (int i = 0; i < ball.length; i++) {
            ball[i] = (Snowball) w.spawnEntity(loc, EntityType.SNOWBALL);
        }
        p.setVelocity(new Vector());
        final Random random = new Random();
        for (int i = 0; i < ball.length; i++) {
            ball[i].setShooter(owner.getPlayer());
            Vector vel = new Vector((random.nextFloat() * 2.0) - 1.0, random.nextFloat(), (random.nextFloat() * 2.0) - 1.0);
            vel.multiply(random.nextFloat());
            ball[i].setVelocity(vel);
        }
        w.createExplosion(loc.getX(), loc.getY(), loc.getZ(), 2F, false, false);
        plate.setType(Material.AIR);
        if (plugin.playersHandling.contains(ownP)) {
            return;
        }
        plugin.playersHandling.add(ownP);
        plugin.fireworkExplosion(p);
        plugin.randomTeleport(p);
        plugin.giveStartItems(ownP);
        plugin.playersHandling.remove(ownP);
        if (plugin.cm.getKillMessageEnabled()) {
            plugin.chatHelper.asend(own, p, "&5&attacker&r hit &5&victim&r!");
        }
        plugin.ksm.addKills(owner, 1);
        plugin.ksm.resetKills(ownP);
    }
}
