/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xxmicloxx.paintball.gameobjects;

import com.xxmicloxx.paintball.OwnPlayer;
import com.xxmicloxx.paintball.Paintball;
import com.xxmicloxx.paintball.events.PaintballPlayerLeaveEvent;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;
import net.minecraft.server.v1_4_R1.Packet62NamedSoundEffect;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_4_R1.CraftSound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

/**
 *
 * @author ML
 */
public class Sniper implements Listener {

    private Paintball plugin;
    public HashMap<OwnPlayer, Integer> fireMap = new HashMap<OwnPlayer, Integer>();

    public Sniper(Paintball plugin) {
        this.plugin = plugin;
        Bukkit.getPluginManager().registerEvents(this, plugin);
        Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {

            @Override
            public void run() {
                LinkedList<OwnPlayer> delList = new LinkedList<OwnPlayer>();
                for (OwnPlayer ownP : fireMap.keySet()) {
                    Player p = ownP.getPlayer();
                    if (p == null) {
                        delList.add(ownP);
                        continue;
                    }
                    int stillToDo = fireMap.get(ownP);
                    final World w = p.getWorld();
                    final Location eyeLoc = p.getEyeLocation();
                    Vector veltemp = eyeLoc.getDirection();
                    final Vector vel = veltemp.multiply(5);
                    Snowball snowBall = (Snowball) w.spawnEntity(eyeLoc, EntityType.SNOWBALL);
                    snowBall.setShooter(p);
                    snowBall.setVelocity(vel);
                    Random random = new Random();
                    w.playSound(eyeLoc, Sound.SHOOT_ARROW, 0.5F, 0.4F / (random.nextFloat() * 0.4F + 0.8F));
                    stillToDo--;
                    if (stillToDo <= 0) {
                        delList.add(ownP);
                    } else {
                        fireMap.put(ownP, stillToDo);
                    }
                }
                for (OwnPlayer p : delList) {
                    fireMap.remove(p);
                }
            }
        }, 0, 1);
    }

    @EventHandler
    public void onPlayerLeave(PaintballPlayerLeaveEvent e) {
        fireMap.remove(e.getPlayer());
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        if (e.getAction() != Action.RIGHT_CLICK_AIR && e.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return;
        }
        Player p = e.getPlayer();
        OwnPlayer ownP = new OwnPlayer(p);
        if (!plugin.arenaList.contains(ownP)) {
            return;
        }
        ItemStack hoe = e.getItem();
        if (hoe == null || hoe.getType() != Material.IRON_HOE) {
            return;
        }
        //checks done, go on with code
        short newDur = (short) (hoe.getDurability() + 11);
        hoe.setDurability(newDur);
        if (newDur >= 251) {
            p.setItemInHand(new ItemStack(Material.AIR));
        }
        Integer integObj = fireMap.get(ownP);
        int i = 0;
        if (integObj != null) {
            i = integObj.intValue();
        }
        i += 3;
        fireMap.put(ownP, i);
    }
}
