/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xxmicloxx.paintball;

import java.io.Serializable;

public class IntFloat implements Serializable{

	private static final long serialVersionUID = 568464244684554L;

	private int intVal;
	private float floatVal;
	public IntFloat(int intVal, float floatVal) {
		this.intVal = intVal;
		this.floatVal = floatVal;
	}
	public int getIntVal() {
		return intVal;
	}
	public void setIntVal(int intVal) {
		this.intVal = intVal;
	}
	public float getFloatVal() {
		return floatVal;
	}
	public void setFloatVal(float floatVal) {
		this.floatVal = floatVal;
	}

}